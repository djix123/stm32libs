#ifndef __DELAY_H__
#define __DELAY_H__

#include "libopencm3/cm3/systick.h"

extern "C" {
    void sys_tick_handler(void);
}

static volatile uint64_t _millis = 0;

void systick_setup(void);

uint64_t millis(void);
void delay(uint64_t duration);

#endif